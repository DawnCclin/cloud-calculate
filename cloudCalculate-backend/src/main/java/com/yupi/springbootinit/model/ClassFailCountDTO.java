package com.yupi.springbootinit.model;

import lombok.Data;

/**
 * @author DawnCclin dawn-lin.xyz @努力的林
 * @description
 * @time 2024/6/19 20:07
 */
@Data
public class ClassFailCountDTO {
    private String className;
    private Double failRatio;
}
     