package com.yupi.springbootinit.model.dto;

import lombok.Data;

/**
 * @author DawnCclin dawn-lin.xyz @努力的林
 * @description
 * @time 2024/6/19 19:48
 */
@Data
public class ClassesAvgDTO {
    private String subjectName;
    private Double averageScore;

}
     