package com.yupi.springbootinit.model.dto;

/**
 * @author DawnCclin dawn-lin.xyz @努力的林
 * @description
 * @time 2024/6/18 23:05
 */
public class CollegeCountDTO {
    private String name;
    private Long value;

    // Getter and Setter methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        {
            this.value = value;
        }
    }
}

     