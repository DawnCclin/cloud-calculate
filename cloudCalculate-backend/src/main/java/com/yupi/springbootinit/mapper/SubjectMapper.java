package com.yupi.springbootinit.mapper;

import com.yupi.springbootinit.model.entity.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13179
* @description 针对表【subject】的数据库操作Mapper
* @createDate 2024-06-17 22:33:19
* @Entity com.yupi.springbootinit.model.entity.subject
*/
public interface SubjectMapper extends BaseMapper<Subject> {

}




