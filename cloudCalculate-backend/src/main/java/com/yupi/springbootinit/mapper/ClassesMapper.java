package com.yupi.springbootinit.mapper;

import com.yupi.springbootinit.model.entity.Classes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13179
* @description 针对表【classes】的数据库操作Mapper
* @createDate 2024-06-17 22:34:52
* @Entity com.yupi.springbootinit.model.entity.classes
*/
public interface ClassesMapper extends BaseMapper<Classes> {

}




