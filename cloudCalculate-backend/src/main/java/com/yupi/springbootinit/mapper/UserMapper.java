package com.yupi.springbootinit.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yupi.springbootinit.model.entity.User;

/**
* @author 13179
* @description 针对表【user(用户)】的数据库操作Mapper
* @createDate 2024-06-17 22:32:37
* @Entity generator.domain.user
*/
public interface UserMapper extends BaseMapper<User> {

}




