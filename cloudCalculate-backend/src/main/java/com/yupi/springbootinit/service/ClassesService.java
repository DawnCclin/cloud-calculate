package com.yupi.springbootinit.service;

import com.yupi.springbootinit.model.entity.Classes;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13179
* @description 针对表【classes】的数据库操作Service
* @createDate 2024-06-17 22:34:52
*/
public interface ClassesService extends IService<Classes> {

}
