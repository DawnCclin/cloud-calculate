package com.yupi.springbootinit.service;

import com.yupi.springbootinit.model.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13179
* @description 针对表【subject】的数据库操作Service
* @createDate 2024-06-17 22:33:19
*/
public interface SubjectService extends IService<Subject> {

}
