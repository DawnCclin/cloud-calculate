package com.yupi.springbootinit.service.impl.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yupi.springbootinit.model.entity.Delayed;
import com.yupi.springbootinit.service.DelayedService;
import com.yupi.springbootinit.mapper.DelayedMapper;
import org.springframework.stereotype.Service;

/**
* @author 13179
* @description 针对表【delayed】的数据库操作Service实现
* @createDate 2024-06-17 22:33:19
*/
@Service
public class DelayedServiceImpl extends ServiceImpl<DelayedMapper, Delayed>
    implements DelayedService {

}




