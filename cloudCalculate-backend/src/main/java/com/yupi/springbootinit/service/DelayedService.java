package com.yupi.springbootinit.service;

import com.yupi.springbootinit.model.entity.Delayed;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13179
* @description 针对表【delayed】的数据库操作Service
* @createDate 2024-06-17 22:33:19
*/
public interface DelayedService extends IService<Delayed> {

}
