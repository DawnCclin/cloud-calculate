package com.yupi.springbootinit.service.impl.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yupi.springbootinit.model.entity.Classes;
import com.yupi.springbootinit.service.ClassesService;
import com.yupi.springbootinit.mapper.ClassesMapper;
import org.springframework.stereotype.Service;

/**
* @author 13179
* @description 针对表【classes】的数据库操作Service实现
* @createDate 2024-06-17 22:34:52
*/
@Service
public class ClassesServiceImpl extends ServiceImpl<ClassesMapper, Classes>
    implements ClassesService {

}




