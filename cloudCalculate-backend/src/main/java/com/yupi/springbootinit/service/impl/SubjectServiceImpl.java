package com.yupi.springbootinit.service.impl.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yupi.springbootinit.model.entity.Subject;
import com.yupi.springbootinit.service.SubjectService;
import com.yupi.springbootinit.mapper.SubjectMapper;
import org.springframework.stereotype.Service;

/**
* @author 13179
* @description 针对表【subject】的数据库操作Service实现
* @createDate 2024-06-17 22:33:19
*/
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject>
    implements SubjectService {

}




