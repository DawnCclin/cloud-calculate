import { PageContainer } from '@ant-design/pro-components';
import {Card} from 'antd';
import React from 'react';


/**
 * 每个单独的卡片，为了复用样式抽成了组件
 * @param param0
 * @returns
 */
const InfoCard: React.FC<{
  title: string;
  index: number;
  desc: string;
  href: string;
}> = ({ title, href, index, desc }) => {
  return (
    <div
      style={{
        backgroundColor: '#FFFFFF',
        boxShadow: '0 2px 4px 0 rgba(35,49,128,0.02), 0 4px 8px 0 rgba(49,69,179,0.02)',
        borderRadius: '8px',
        fontSize: '14px',
        color: 'rgba(0,0,0,0.65)',
        textAlign: 'justify',
        lineHeight: ' 22px',
        padding: '16px 19px',
        flex: 1,
      }}
    >
      <div
        style={{
          display: 'flex',
          gap: '4px',
          alignItems: 'center',
        }}
      >
        <div
          style={{
            width: 48,
            height: 48,
            lineHeight: '22px',
            backgroundSize: '100%',
            textAlign: 'center',
            padding: '8px 16px 16px 12px',
            color: '#FFF',
            fontWeight: 'bold',
            backgroundImage:
              "url('https://gw.alipayobjects.com/zos/bmw-prod/daaf8d50-8e6d-4251-905d-676a24ddfa12.svg')",
          }}
        >
          {index}
        </div>
        <div
          style={{
            fontSize: '16px',
            color: 'rgba(0, 0, 0, 0.85)',
            paddingBottom: 8,
          }}
        >
          {title}
        </div>
      </div>
      <div
        style={{
          fontSize: '14px',
          color: 'rgba(0,0,0,0.65)',
          textAlign: 'justify',
          lineHeight: '22px',
          marginBottom: 8,
        }}
      >
        {desc}
      </div>
      <a href={href} target="_blank" rel="noreferrer">
        了解更多 {'>'}
      </a>
    </div>
  );
};


const Welcome: React.FC = () => {
  return (
    <PageContainer>
        <Card
        style={{
        borderRadius: 8,
        }}
        bodyStyle={{
        backgroundImage:
        'radial-gradient(circle at 97% 10%, #EBF2FF 0%, #F5F8FF 28%, #EBF1FF 124%)',
        }}
        >
        <div
        style={{
        backgroundPosition: '100% -30%',
        backgroundRepeat: 'no-repeat',
        backgroundSize: '274px auto',
        backgroundImage:
        "url('https://gw.alipayobjects.com/mdn/rms_a9745b/afts/img/A*BuFmQqsB2iAAAAAAAAAAAAAAARQnAQ')",
        }}
        >
        <div
        style={{
        fontSize: '20px',
        color: '#1A1A1A',
        }}
        >
        欢迎使用 云计算 成绩分析 开放平台
      </div>
          <p
            style={{
              fontSize: '14px',
              color: 'rgba(0,0,0,0.65)',
              lineHeight: '22px',
              marginTop: 16,
              marginBottom: 32,
              width: '65%',
            }}
          >
            本平台是一款先进的云计算解决方案，专为教育领域设计，旨在实现
            学生成绩数据的高效、实时分析与管理。它集成了强大的数据处理与
            分析能力，能够帮助教师、教育管理者以及学生本人深入了解学习成
            效，优化教学策略与个人学习计划。<br/><br/>
            <h3>1. 实时数据导入与分析：</h3>
            平台支持多种数据格式一键导入，包括Excel、CSV等，实现成绩数据的快速整合。<br/>
            实时分析引擎能够即刻处理导入的数据，无需等待即可查看成绩分布、趋势分析等关键指标。<br/>
            <h3>2. 多维度成绩分析：</h3>
            提供丰富的图表展示，如分数分布直方图、趋势线图、雷达图等，直观展现班级、学科成绩对比。<br/>
            支持按学生、科目、考试类型等多维度深入分析，快速定位学习强项与薄弱环节。<br/>
            <h3>3. 个性化学习建议：</h3>
            根据学生的学习表现，智能生成个性化提升建议，助力学生有的放矢地改进学习方法。<br/>
            为教师提供班级整体及个体学生的教学反馈，辅助制定差异化教学方案。<br/>
            <h3>4. 安全与隐私保护：</h3>
            采用阿里云高级安全技术，确保所有数据传输与存储过程中的安全性与隐私保护。<br/>
            遵循严格的隐私政策，确保用户数据仅用于教育分析目的，维护用户信任。<br/>
            <h3>5. 易于操作的界面：</h3>
            设计简洁直观的操作界面，无论是技术专家还是非技术人员都能轻松上手。<br/>
            支持跨平台访问，无论是PC端还是移动设备，都能随时随地查看和分析成绩数据。<br/>
          </p>
          <div
        style={{
          display: 'flex',
          gap: 16,
        }}
      >
  <InfoCard
    index={1}
    href="http://dawn-lin.xyz/about"
    title="了解 作者"
    desc="作者个人信息~"
  />
  <InfoCard
    index={2}
    title="了解 作者博客网站"
    href="http://dawn-lin.xyz"
    desc="作者的个人博客网站，在这里你可以看到作者的一些项目，以及一些技术文章，包括一些学习经验等。"
  />
</div>
</div>
</Card>
</PageContainer>
);
};
export default Welcome;
