﻿export default [
  {
    path: '/user',
    layout: false,
    routes: [{ name: '登录', path: '/user/login', component: './User/Login' }],
  },
  { path: '/welcome', name: '欢迎', icon: 'smile', component: './index' },
  {
    path: '/admin',
    name: '管理页',
    icon: 'crown',
    routes: [
      { path: '/admin', redirect: '/admin/sub-page' },
      { path: '/admin/sub-page', name: '管理页', component: './Admin' },
    ],
  },
  { name: '缓考的学院分布饼状图', icon: 'table', path: '/list1', component: './classCalculate/list1' },
  { name: '缓考原因饼状图', icon: 'table', path: '/list2', component: './classCalculate/list2' },
  { name: '任一门课成绩分析', icon: 'table', path: '/list3', component: './classCalculate/list3' },
  { name: '平时成绩与挂科率分析', icon: 'table', path: '/list4', component: './classCalculate/list4' },
  { name: '各科成绩分析', icon: 'table', path: '/list5', component: './classCalculate/list5' },
  { name: '各班挂科率对比', icon: 'table', path: '/list6', component: './classCalculate/list6' },
  { name: '男女生各科成绩差异', icon: 'table', path: '/list7', component: './classCalculate/list7' },
  { name: '任课老师挂科率和平时分的分析', icon: 'table', path: '/list8', component: './classCalculate/list8' },
  // { name: '专业课与公共课平时成绩与总评对比', icon: 'table', path: '/list9', component: './classCalculate/list9' },
  { path: '/', redirect: '/welcome' },
  { path: '*', layout: false, component: './404' },
];
